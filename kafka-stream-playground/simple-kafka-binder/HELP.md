### EAD

#### Pub/Sub model:

#### EventStream model (different EventSourcing)
* https://faun.pub/event-driven-application-using-spring-cloud-stream-c1a97eb81427

#### Testing
```bash
curl -d '{"id":"1", "name":"my name", "data":"some data"}' -H "Content-Type: application/json" -X POST http://localhost:8080/api/event
curl -d "@data.json" -X POST http://localhost:8080/api/event
```