package com.example.simplekafkabinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleKafkaBinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleKafkaBinderApplication.class, args);
	}

}
