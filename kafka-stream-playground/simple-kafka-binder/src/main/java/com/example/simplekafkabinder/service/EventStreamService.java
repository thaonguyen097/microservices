package com.example.simplekafkabinder.service;

import com.example.simplekafkabinder.config.EventStream;
import com.example.simplekafkabinder.dto.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

@Service
@RequiredArgsConstructor
public class EventStreamService {
	private final EventStream eventStream;

	public Boolean produceEvent(Message msg) {
		System.out.println("Producing events --> id: "+ msg.getId() +" name: "+msg.getName()+" Actual message: "+ msg.getData());
		msg.setBytePayload(msg.getData().getBytes());
		MessageChannel messageChannel = eventStream.producer();
		return messageChannel.send(MessageBuilder.withPayload(msg)
				.setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON).build());
		
	}

}