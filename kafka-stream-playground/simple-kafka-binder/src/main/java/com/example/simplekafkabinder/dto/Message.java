package com.example.simplekafkabinder.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Message {
	private Integer id;
	private String name;
	private String data;
	private byte[] bytePayload;

}