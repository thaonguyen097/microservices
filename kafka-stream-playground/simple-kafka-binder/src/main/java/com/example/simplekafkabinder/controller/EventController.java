package com.example.simplekafkabinder.controller;

import com.example.simplekafkabinder.dto.Message;
import com.example.simplekafkabinder.service.EventStreamService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/event")
@RequiredArgsConstructor
public class EventController {
	private final EventStreamService eventStreamService;

	@PostMapping("/produce")
	public Boolean sendEvent(@RequestBody Message msg) throws Exception {
		return eventStreamService.produceEvent(msg);
	}

}