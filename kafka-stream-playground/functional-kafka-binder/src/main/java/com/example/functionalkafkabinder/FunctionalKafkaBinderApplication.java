package com.example.functionalkafkabinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FunctionalKafkaBinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(FunctionalKafkaBinderApplication.class, args);
	}

}
