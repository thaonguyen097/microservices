package com.example.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class BookConsumer2Application {
    
    public static void main(String[] args) {
        SpringApplication.run(BookConsumer2Application.class, args);
    }
    
}

