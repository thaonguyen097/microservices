package com.example.book;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@FeignClient(name = "book-client",
        url = "http://localhost:9000", path = "api",
        fallback = BookServiceProxyFallback.class)
public interface BookServiceProxy {

    @RequestMapping(method = RequestMethod.GET, value = "/books")
    List<Book> getBooks();

}

class BookServiceProxyFallback implements BookServiceProxy {
    @Override
    public List<Book> getBooks() {
        return new ArrayList<>();
    }
}
