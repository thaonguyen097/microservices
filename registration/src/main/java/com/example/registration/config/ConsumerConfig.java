package com.example.registration.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;

import com.example.registration.dto.Submission;

import reactor.kafka.receiver.ReceiverOptions;

import java.util.Collections;

@Configuration
public class ConsumerConfig {
	
    @Bean
    public ReceiverOptions<String, Submission> kafkaReceiverOptions(@Value(value = "${FAKE_CONSUMER_DTO_TOPIC}") String topic, KafkaProperties kafkaProperties) {
        ReceiverOptions<String, Submission> basicReceiverOptions = ReceiverOptions.create(kafkaProperties.buildConsumerProperties());
        return basicReceiverOptions.subscription(Collections.singletonList(topic));
    }

    @Bean
    public ReactiveKafkaConsumerTemplate<String, Submission> reactiveKafkaConsumerTemplate(ReceiverOptions<String, Submission> kafkaReceiverOptions) {
        return new ReactiveKafkaConsumerTemplate<String, Submission>(kafkaReceiverOptions);
    }
}