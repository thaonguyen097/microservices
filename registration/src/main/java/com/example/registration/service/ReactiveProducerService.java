package com.example.registration.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;

import com.example.registration.dto.Enrollment;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class ReactiveProducerService {

    private final ReactiveKafkaProducerTemplate<String, Enrollment> producerTemplate;

    @Value(value = "${FAKE_PRODUCER_DTO_TOPIC}")
    private String topic;

    public void send(Enrollment enrollment) {
        log.info("send to topic={}, {}={},", topic, Enrollment.class.getSimpleName(), enrollment);
        producerTemplate.send(topic, enrollment)
                .doOnSuccess(senderResult -> log.info("sent {} offset : {}", enrollment, senderResult.recordMetadata().offset()))
                .subscribe();
    }
}