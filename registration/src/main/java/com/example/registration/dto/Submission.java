package com.example.registration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Data;

@JsonRootName("SubmissionConsumer")
@Data
@AllArgsConstructor
public class Submission {
	
    @JsonProperty("id")
    private String id;

    @JsonProperty("enrollDate")
    private String enrollDate;
    
    @JsonProperty("studentCode")
    private String studentCode;
    
    @JsonProperty("courseName")
    private String courseName;

	@Override
	public String toString() {
		return "SubmissionConsumer [id=" + id + ", enrollDate=" + enrollDate + ", studentCode=" + studentCode + ", courseName="
				+ courseName + "]";
	}
    
    
}