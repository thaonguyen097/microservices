package com.example.registration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Data;

@JsonRootName("EnrollmentProducer")
@Data
@AllArgsConstructor
public class Enrollment {
	
    @JsonProperty("id")
    private String id;
    
    @JsonProperty("enrollDate")
    private String enrollDate;
    
    @JsonProperty("studentCode")
    private String studentCode;
    
    @JsonProperty("courseName")
    private String courseName;

	@Override
	public String toString() {
		return "EnrollmentProducer [id=" + id + ", enrollDate=" + enrollDate + ", studentCode=" + studentCode + ", courseName="
				+ courseName + "]";
	}

    
}