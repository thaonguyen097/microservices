package com.example.registration;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@EmbeddedKafka(topics = {"${FAKE_PRODUCER_DTO_TOPIC}", "${FAKE_CONSUMER_DTO_TOPIC}"})
class RegistrationApplicationTests {

	@Test
	void contextLoads() {
		assertDoesNotThrow(() -> RegistrationApplication.main(new String[]{"--server.port=0"}));
	}

}
