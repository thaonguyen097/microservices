package com.example.customer.client.proxy;

import com.example.customer.client.dto.CustomerDto;

import java.util.List;
import java.util.Optional;

public interface HttpService {

    /**
     * @return customers
     */
    List<CustomerDto> findAll();

    /**
     * @param name
     * @return customers
     */
    List<CustomerDto> findByName(String name);

    /**
     * @param id
     * @return customer
     */
    Optional<CustomerDto> findById(long id);

    /**
     * @param customer
     * @return customer
     */
    CustomerDto save(CustomerDto customer);

    /**
     * @param id
     * @param customer
     * @return customer
     */
    CustomerDto update(long id, CustomerDto customer);

    /**
     * @param customerId
     */
    void deleteById(long customerId);

}
