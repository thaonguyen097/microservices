package com.example.customer.client.proxy;

import com.example.customer.client.dto.CustomerDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class CustomerProxy implements HttpService {

    private final RestTemplate restTemplate;

    @Value("${app.customer.uri}")
    private String customerUri;

    /**
     * @return customers
     */
    @Override
    public List<CustomerDto> findAll() {
        return getCustomers(null);
    }

    /**
     * @param name
     * @return customers
     */
    @Override
    public List<CustomerDto> findByName(final String name) {
        return getCustomers(Map.of("name", name));
    }

    /**
     * @param id
     * @return customer
     */
    @Override
    public Optional<CustomerDto> findById(final long id) {
        final HttpEntity request = createHttpEntity(null);
        final ParameterizedTypeReference<Optional<CustomerDto>> typeRef = new ParameterizedTypeReference<>() { };
        final ResponseEntity<Optional<CustomerDto>> response =
                restTemplate.exchange(customerUri + "/{customerId}", HttpMethod.GET,
                        request, typeRef, id);
        return response.getBody();
    }

    /**
     * @param customer
     * @return customer
     */
    @Override
    public CustomerDto save(final CustomerDto customer) {
        final HttpEntity request = createHttpEntity(customer);
        final ResponseEntity<CustomerDto> response = restTemplate
                .exchange(customerUri, HttpMethod.POST, request, CustomerDto.class);
        return response.getBody();
    }

    /**
     * @param customerId
     * @param customer
     * @return customer
     */
    @Override
    public CustomerDto update(final long customerId, final CustomerDto customer) {
        final HttpEntity request = createHttpEntity(customer);
        final ResponseEntity<CustomerDto> response =
                restTemplate.exchange(customerUri + "/{customerId}", HttpMethod.POST,
                        request, CustomerDto.class, customerId);
        return response.getBody();
    }

    /**
     * @param customerId
     */
    @Override
    public void deleteById(final long customerId) {
        final HttpEntity request = createHttpEntity(null);
        restTemplate.exchange(customerUri + "/{customerId}", HttpMethod.POST,
                        request, Void.class, customerId);
    }

    private List<CustomerDto> getCustomers(final Map<String, Object> variables) {
        final HttpEntity request = createHttpEntity(null);
        final ParameterizedTypeReference<List<CustomerDto>> typeRef = new ParameterizedTypeReference<>() { };
        final ResponseEntity<List<CustomerDto>> response = Objects.isNull(variables)
                ? restTemplate.exchange(customerUri, HttpMethod.GET, request, typeRef)
                : restTemplate.exchange(customerUri, HttpMethod.GET, request, typeRef, variables);
        return response.getBody();
    }

    private <T> HttpEntity<T> createHttpEntity(final T body) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return Objects.isNull(body) ? new HttpEntity(headers) : new HttpEntity(body, headers);
    }

}
