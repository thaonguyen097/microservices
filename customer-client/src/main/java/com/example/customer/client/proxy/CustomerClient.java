package com.example.customer.client.proxy;

import com.example.customer.client.dto.CustomerDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;
import java.util.Optional;

@FeignClient(name = "customerFeignClient", url = "${app.customer.uri}")
public interface CustomerClient extends HttpService {

     @GetMapping
     @Override
     List<CustomerDto> findAll();

     @GetMapping
     @Override
     List<CustomerDto> findByName(@RequestParam String name);

     @GetMapping("/{customerId}")
     @Override
     Optional<CustomerDto> findById(@PathVariable long customerId);

     @PostMapping
     @Override
     CustomerDto save(CustomerDto customer);

     @PutMapping("/{customerId}")
     @Override
     CustomerDto update(@PathVariable long customerId, CustomerDto customer);

     @DeleteMapping("/{customerId}")
     @Override
     void deleteById(@PathVariable long customerId);

}
