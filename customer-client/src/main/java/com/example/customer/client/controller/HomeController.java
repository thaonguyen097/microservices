package com.example.customer.client.controller;

import com.example.customer.client.dto.CustomerDto;
import com.example.customer.client.proxy.CustomerClient;
import com.example.customer.client.proxy.CustomerProxy;
import com.example.customer.client.proxy.HttpService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/customers")
@RequiredArgsConstructor
public class HomeController {

    private final CustomerProxy custSrv;
    private final CustomerClient simpleCustSrv;

    private static final String CUSTOMER_NESTED = "{customerId}";
    private static final String VERSION_1 = "x-api-version=1";
    private static final String VERSION_2 = "application/vnd.company.app-v1+json";

    /**
     * @param name
     * @return resp
     */
    @GetMapping(headers = VERSION_1)
    public ResponseEntity<List<CustomerDto>> getCustomersByHeader(final @RequestParam(required = false) String name) {
        return getCustomers(simpleCustSrv, name);
    }

    /**
     * @param name
     * @return resp
     */
    @GetMapping(produces = VERSION_2)
    public ResponseEntity<List<CustomerDto>> getCustomersByMediaType(
            final @RequestParam(required = false) String name) {
        return getCustomers(custSrv, name);
    }

    private ResponseEntity<List<CustomerDto>> getCustomers(final HttpService httpService,
                                                             final String name) {
        ResponseEntity<List<CustomerDto>> resp;
        if (StringUtils.hasLength(name)) {
            resp = ResponseEntity.status(HttpStatus.OK).body(httpService.findByName(name.trim()));
        } else {
            resp = ResponseEntity.status(HttpStatus.OK).body(httpService.findAll());
        }
        return resp;
    }

    /**
     * @param customerId
     * @return resp
     */
    @GetMapping(value = CUSTOMER_NESTED, headers = VERSION_1)
    public ResponseEntity<CustomerDto> getCustomerByHeader(final @PathVariable long customerId) {
        return getCustomer(simpleCustSrv, customerId);
    }

    /**
     * @param customerId
     * @return resp
     */
    @GetMapping(value = CUSTOMER_NESTED, produces = VERSION_2)
    public ResponseEntity<CustomerDto> getCustomerByMediaType(final @PathVariable long customerId) {
        return getCustomer(custSrv, customerId);
    }

    private ResponseEntity<CustomerDto> getCustomer(final HttpService httpService,
                                                    final long id) {
        return httpService.findById(id).map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * @param customer
     * @return response
     */
    @PostMapping(headers = VERSION_1)
    public ResponseEntity<CustomerDto> createEmployeeByHeader(final @RequestBody @Valid CustomerDto customer) {
        return createEmployee(simpleCustSrv, customer);
    }

    /**
     * @param customer
     * @return response
     */
    @PostMapping(produces = VERSION_2)
    public ResponseEntity<CustomerDto> createEmployeeByMediaType(final @RequestBody @Valid CustomerDto customer) {
        return createEmployee(custSrv, customer);
    }

    private ResponseEntity<CustomerDto> createEmployee(final HttpService httpService,
                                                      final CustomerDto customer) {
        return ResponseEntity.status(HttpStatus.CREATED).body(httpService.save(customer));
    }

    /**
     * @param customerId
     * @param customer
     * @return resp
     */
    @PutMapping(value = CUSTOMER_NESTED, headers = VERSION_1)
    public ResponseEntity<CustomerDto> updateCustomerByHeader(final @PathVariable long customerId,
                                                      final @RequestBody @Valid CustomerDto customer) {
        return updateCustomer(simpleCustSrv, customerId, customer);
    }

    /**
     * @param customerId
     * @param customer
     * @return resp
     */
    @PutMapping(value = CUSTOMER_NESTED, produces = VERSION_2)
    public ResponseEntity<CustomerDto> updateCustomerByMediaType(final @PathVariable long customerId,
                                                      final @RequestBody @Valid CustomerDto customer) {
        return updateCustomer(custSrv, customerId, customer);
    }

    private ResponseEntity<CustomerDto> updateCustomer(final HttpService httpService,
                                                      final long id,
                                                      final CustomerDto customer) {
        return httpService.findById(id).map(dbCustomer -> {
            dbCustomer.setName(customer.getName());
            dbCustomer.setEmail(customer.getEmail());
            return ResponseEntity.accepted().body(httpService.update(id, dbCustomer));
        }).orElseThrow(() -> new IllegalArgumentException("Customer id " + id + " not found."));
    }

    /**
     * @param customerId
     * @return resp
     */
    @DeleteMapping(value = CUSTOMER_NESTED, headers = VERSION_1)
    public ResponseEntity<String> deleteEmployeeByHeader(final @PathVariable long customerId) {
        return deleteEmployee(simpleCustSrv, customerId);
    }

    /**
     * @param customerId
     * @return resp
     */
    @DeleteMapping(value = CUSTOMER_NESTED, produces = VERSION_2)
    public ResponseEntity<String> deleteEmployeeByMediaType(final @PathVariable long customerId) {
        return deleteEmployee(custSrv, customerId);
    }

    private ResponseEntity<String> deleteEmployee(final HttpService httpService,
                                                  final long id) {
        return httpService.findById(id).map(customer -> {
            httpService.deleteById(customer.getId());
            return ResponseEntity.accepted().body("Deleted");
        }).orElseThrow(() -> new IllegalArgumentException("Customer id " + id + " not found."));
    }

}
