package com.example.customer.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CustomerDemoApplication {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        SpringApplication.run(CustomerDemoApplication.class, args);
    }

}
