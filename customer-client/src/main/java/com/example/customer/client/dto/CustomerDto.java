package com.example.customer.client.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class CustomerDto {

    private static final int NAME_MAX = 50;
    private static final int NAME_MIN = 2;

    private Long id;

    @NotBlank
    @Size(min = NAME_MIN, max = NAME_MAX)
    private String name;

    @NotBlank
    @Email
    private String email;
}
