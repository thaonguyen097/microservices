package com.example.customer.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
public class RestConfig {

    @Value("${app.customer.timeout:500}")
    private long timeout;

    // https://hellokoding.com/spring-resttemplate-timeout/
    // https://medium.com/@TimvanBaarsen
    // /spring-boot-why-you-should-always-use-the-resttemplatebuilder-to-create
    // -a-resttemplate-instance-d5a44ebad9e9

    /**
     * @param restTplBld
     * @return RestTemplate
     */
    @Bean
    public RestTemplate restTemplate(final RestTemplateBuilder restTplBld) {
        return restTplBld
                .setConnectTimeout(Duration.ofMillis(timeout))
                .setReadTimeout(Duration.ofMillis(timeout))
                .build();
    }

}
