package com.example.customer.client;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CustomerDemoApplicationTests {


	@Test
	void contextLoads() {

	}

	@Test
	public void applicationContextTest() {
		CustomerDemoApplication.main(new String[] {});
	}

}
