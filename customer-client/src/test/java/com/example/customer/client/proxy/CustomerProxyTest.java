package com.example.customer.client.proxy;

import com.example.customer.client.dto.CustomerDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomerProxyTest {

    @InjectMocks
    private CustomerProxy sut;

    @Mock
    private RestTemplate restTemplate;

    @BeforeEach
    public void setUp() {
        ReflectionTestUtils.setField(sut, "customerUri", "my url");
    }

    @Test
    @DisplayName("find all test")
    void findAllTest() {

        when(restTemplate.exchange(anyString(), any(HttpMethod.class),
            any(HttpEntity.class), any(ParameterizedTypeReference.class)))
            .thenReturn(ResponseEntity.ok(Arrays.asList(CustomerDto.builder()
                .id(1l).name("tom").email("tom@mail.com")
                .build()))
        );

        List<CustomerDto> actual = sut.findAll();

        assertAll(
                () -> assertEquals(1, actual.size()),
                () -> assertEquals("tom", actual.get(0).getName())
        );
    }

}