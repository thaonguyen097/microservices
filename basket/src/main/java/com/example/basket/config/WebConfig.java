package com.example.basket.config;

import static org.springframework.web.servlet.function.RequestPredicates.GET;
import static org.springframework.web.servlet.function.RouterFunctions.route;
import static org.springframework.web.servlet.function.ServerResponse.ok;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.function.RouterFunction;
import org.springframework.web.servlet.function.ServerResponse;

@Configuration
public class WebConfig {
    
    @Bean
    public RouterFunction<ServerResponse> routes() {
        return route(GET("/"), (req) -> ok().body("basket service ready"));
    }

  

}
