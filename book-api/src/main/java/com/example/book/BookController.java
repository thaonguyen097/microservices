package com.example.book;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
@RequestMapping("/api/books")
public class BookController {

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping
    public JsonNode getRandomBooks() {

        Faker faker = new Faker(new Locale("en-US"));
        ArrayNode books = objectMapper.createArrayNode();

        for (int i = 0; i < 10; i++) {
            books.add(objectMapper.createObjectNode()
                    .put("author", faker.book().author())
                    .put("genre", faker.book().genre())
                    .put("publisher", faker.book().publisher())
                    .put("title", faker.book().title()));
        }

        return books;
    }
}
