package com.example.catalog.web;

import static org.springframework.web.servlet.function.ServerResponse.noContent;
import static org.springframework.web.servlet.function.ServerResponse.notFound;
import static org.springframework.web.servlet.function.ServerResponse.ok;

import java.io.IOException;

import javax.servlet.ServletException;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;

import com.example.catalog.Product;
import com.example.catalog.dao.ProductDao;
import com.example.catalog.service.KafkaProducer;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class ProductHandler {

	private final ProductDao productDao;
	private final KafkaProducer producer;
	

	public ServerResponse all(ServerRequest req) {
		return ok().body(this.productDao.findAll());
	}

	public ServerResponse update(ServerRequest req) throws ServletException, IOException {

		var data = req.body(Product.class);

		return this.productDao.findById(Long.valueOf(req.pathVariable("id"))).map(product -> {
			product.setProductName(data.getProductName());
			product.setPrice(data.getPrice());
			return product;
		}).map(this.productDao::save).map(product -> {
			producer.publishEvent(product);
			return noContent().build();
		}).orElse(notFound().build());

	}

	

}