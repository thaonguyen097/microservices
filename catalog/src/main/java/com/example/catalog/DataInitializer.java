package com.example.catalog;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.example.catalog.dao.ProductDao;

import java.util.Random;
import java.util.stream.Stream;

@Component
@Slf4j
@RequiredArgsConstructor
public class DataInitializer implements ApplicationRunner {
    
    private final ProductDao productDao;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        this.initPosts();
    }
    
    private void initPosts() {
    	Random rand = new Random(); 
        log.info(" start data initializing...");
        this.productDao.deleteAll();
        Stream.of("Product 1", "Product 2").forEach(
                title -> this.productDao.save(Product.builder().productName(title).price(rand.nextDouble(10)).build())
        );
        log.info(" done data initialization...");
        log.info(" initialized data::");
        this.productDao.findAll().forEach(p -> log.info(p.toString()));
    }
}