package com.example.catalog.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.catalog.Product;

public interface ProductDao extends JpaRepository<Product, Long> {

}
