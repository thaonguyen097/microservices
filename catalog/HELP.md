# Getting Started

### Start kafka
```bash
$ bin/zookeeper-server-start.sh config/zookeeper.properties
$ bin/kafka-server-start.sh config/server.properties
```

### Kafka playground
```bash
bin/kafka-topics.sh --create --topic quickstart-events --bootstrap-server localhost:9092
bin/kafka-topics.sh --describe --topic quickstart-events --bootstrap-server localhost:9092
# Topic:quickstart-events  PartitionCount:1    ReplicationFactor:1 Configs:
#    Topic: quickstart-events Partition: 0    Leader: 0   Replicas: 0 Isr: 0
bin/kafka-console-producer.sh --topic quickstart-events --bootstrap-server localhost:9092
bin/kafka-console-consumer.sh --topic quickstart-events --from-beginning --bootstrap-server localhost:9092
```


