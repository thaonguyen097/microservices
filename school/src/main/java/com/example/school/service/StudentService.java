package com.example.school.service;

import org.springframework.stereotype.Service;

import com.example.school.dao.StudentRepo;
import com.example.school.dao.entity.StudentEntity;
import com.example.school.dto.Student;
import com.example.school.dto.StudentMapper;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class StudentService {
	
	private final StudentMapper studentMapper;
	private final StudentRepo studentRepo;
	
	public Mono<Student> saveStudent(Student student) {
		final StudentEntity e = studentMapper.toEntity(student);
        return studentRepo.save(e).map(studentMapper::toModel);
    }

    public Mono<Student> getStudent(Long studentId) {
        return studentRepo.findById(studentId)
                .map(studentMapper::toModel);
    }
    
    public Flux<Student> getAllStudents() {
        return studentRepo.findAll()
                .map(studentMapper::toModel);
    }

    public Mono<Student> updateStudent(Long id, Student student) {
    	final StudentEntity e = studentMapper.toEntity(student);
        return studentRepo.findById(id)
	        .flatMap(s -> {
	            e.setId(s.getId());
	            return studentRepo.save(e).map(studentMapper::toModel);
	        });

    }

    public Mono<Void> deleteStudent(Long studentId) {
        return studentRepo.deleteById(studentId);
    }

}
