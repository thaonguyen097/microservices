package com.example.school;

import java.util.stream.Stream;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.r2dbc.connection.init.ConnectionFactoryInitializer;
import org.springframework.r2dbc.connection.init.ResourceDatabasePopulator;
import org.springframework.r2dbc.core.DatabaseClient;

import com.example.school.dao.StudentRepo;
import com.example.school.dao.entity.StudentEntity;

import io.r2dbc.spi.ConnectionFactory;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class SchoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolApplication.class, args);
	}
	
	//@Bean
    ConnectionFactoryInitializer initializer(ConnectionFactory connectionFactory) {

        ConnectionFactoryInitializer initializer = new ConnectionFactoryInitializer();
        initializer.setConnectionFactory(connectionFactory);
        initializer.setDatabasePopulator(new ResourceDatabasePopulator(new ClassPathResource("schema.sql")));

        return initializer;
    }
	
	@Bean
    ApplicationRunner init(StudentRepo repository, DatabaseClient client) {
        return args -> {
            client.sql("create table student(id bigint auto_increment, student_name varchar(255));")
            	.fetch().first().subscribe();
            client.sql("delete from student;")
            	.fetch().first().subscribe();

            Stream<StudentEntity> stream = Stream.of(new StudentEntity(null, "Hi this is my first todo!"),
                    new StudentEntity(null, "This one I have acomplished!"),
                    new StudentEntity(null, "And this is secret"));

            // initialize the database

            repository.saveAll(Flux.fromStream(stream))
                    .then()
                    .subscribe(); // execute

        };
    }

}
