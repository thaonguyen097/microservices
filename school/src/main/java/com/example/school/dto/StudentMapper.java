package com.example.school.dto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.example.school.dao.entity.StudentEntity;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StudentMapper {
	
	@Mappings({ @Mapping(source = "studentName", target = "name") })
	Student toModel(StudentEntity stud);

	@Mappings({ @Mapping(source = "name", target = "studentName") })
	StudentEntity toEntity(Student stud);
}
