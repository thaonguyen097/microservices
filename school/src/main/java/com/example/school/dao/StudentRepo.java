package com.example.school.dao;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.example.school.dao.entity.StudentEntity;

import reactor.core.publisher.Flux;

@Repository
public interface StudentRepo extends ReactiveCrudRepository<StudentEntity, Long> {

	Flux<StudentEntity> findByStudentName(String name);
}
