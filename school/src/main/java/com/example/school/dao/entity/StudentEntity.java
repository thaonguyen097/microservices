package com.example.school.dao.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table("student")
public class StudentEntity {
	
	@Id
	@Column
	private Long id;
	
	@Column("student_name")
	private String studentName;
	

}
