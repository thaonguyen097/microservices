package com.example.school.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.PUT;
import static org.springframework.web.reactive.function.server.RequestPredicates.DELETE;

@Configuration
public class StudentRouter {

	@Bean
	public RouterFunction<ServerResponse> route(StudentHandler handler) {

		return RouterFunctions
				.route(POST("/v2/students").and(RequestPredicates.accept(MediaType.APPLICATION_JSON)),
						handler::createNewStudent)
				.andRoute(GET("/v2/students/{studentId}").and(RequestPredicates.accept(MediaType.APPLICATION_JSON)),
						handler::getStudent)
				.andRoute(PUT("/v2/students/{studentId}").and(RequestPredicates.accept(MediaType.APPLICATION_JSON)),
						handler::updateStudent)
				.andRoute(DELETE("/v2/students/{studentId}").and(RequestPredicates.accept(MediaType.APPLICATION_JSON)),
						handler::deleteStudent)
				.andRoute(GET("/v2/students").and(RequestPredicates.accept(MediaType.APPLICATION_JSON)),
						handler::getListStudents);
	}

}
