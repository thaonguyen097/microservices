package com.example.school.web;

import java.net.URI;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.example.school.dao.StudentRepo;
import com.example.school.dao.entity.StudentEntity;
import com.example.school.dto.Student;
import com.example.school.dto.StudentMapper;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class StudentHandler {

	private final StudentMapper studentMapper;
	private final StudentRepo studentRepo;

	public Mono<ServerResponse> getListStudents(ServerRequest request) {
		Flux<Student> students = studentRepo.findAll().map(studentMapper::toModel);
		return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(students, Student.class);
	}

	public Mono<ServerResponse> getStudent(ServerRequest request) {
		String studentId = request.pathVariable("studentId");
		Mono<Student> studentMono = studentRepo.findById(Long.valueOf(studentId)).map(studentMapper::toModel);
		return studentMono.flatMap(p -> ServerResponse.ok().body(Mono.just(p), Student.class))
         .switchIfEmpty(ServerResponse.notFound().build());
	}

	public Mono<ServerResponse> createNewStudent(ServerRequest request) {
		Mono<StudentEntity> studentMono = request.bodyToMono(Student.class).map(studentMapper::toEntity);
		return studentMono.flatMap(student -> studentRepo.save(student).map(studentMapper::toModel))
	            .flatMap(p -> ServerResponse.created(URI.create("/v2/student/" + p.getId())).build());
	}
	
	public Mono<ServerResponse> updateStudent(ServerRequest request) {
		String studentId = request.pathVariable("studentId");
        return Mono
            .zip(
                (data) -> {
                    StudentEntity p = (StudentEntity) data[0];
                    Student p2 = (Student) data[1];
                    p.setStudentName(p2.getName());
                    return p;
                },
                studentRepo.findById(Long.valueOf(studentId)),
                request.bodyToMono(Student.class)
            )
            .cast(StudentEntity.class)
            .flatMap(p -> studentRepo.save(p))
            .flatMap(p -> ServerResponse.noContent().build());

    }

	public Mono<ServerResponse> deleteStudent(ServerRequest request) {
		String studentId = request.pathVariable("studentId");
		Mono<Void> employeeMono = studentRepo.deleteById(Long.valueOf(studentId));		
        return ServerResponse.noContent().build(employeeMono);

	}

}
