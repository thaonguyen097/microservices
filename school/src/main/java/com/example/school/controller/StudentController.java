package com.example.school.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.school.dto.Student;
import com.example.school.service.StudentService;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1/students")
@RequiredArgsConstructor
public class StudentController {
	
	private final StudentService studentService;
	
	@PostMapping
    public Mono<Student> saveStudent(final @RequestBody Student student) {
        return studentService.saveStudent(student);
    }

    @GetMapping(value = "/{studentId}")
    public Mono<ResponseEntity<Student>> getStudent(final @PathVariable Long studentId) {
        return studentService.getStudent(studentId)
        		.map(ResponseEntity::ok)
        		.defaultIfEmpty(ResponseEntity.notFound().build());
    }
    
    @GetMapping
    public Flux<Student> getStudents() {
        return studentService.getAllStudents();
    }
    
    @PutMapping(value = "/{studentId}")
    public Mono<ResponseEntity<Student>> updateStudent(final @PathVariable Long studentId, 
    		final @RequestBody Student student) {
    	return studentService.updateStudent(studentId, student)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
    
    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteStudent(final @PathVariable Long studentId) {
        return studentService.getStudent(studentId)
                .flatMap(s ->
                        studentService.deleteStudent(s.getId())
                        .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK)))
                )
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
