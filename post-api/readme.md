- Create account Auth0
- Create API, add permission
- Update Spring config

```bash
curl --request POST \
--url https://dev-bxebbusy.us.auth0.com/oauth/token \
--header 'content-type: application/json' \
--data '{"client_id":"Gpx90rLJMFNTyZ17J0RyqV3bUYfNNLsU","client_secret":"LWhfhttmm5eVBCQShTYmJkVFGUi-6XKG__FL9xcUCVKASvzbNTF86qiZ8ckq1odE","audience":"https://post-api.example.com/","grant_type":"client_credentials"}'
```

- Check token https://jwt.io/
```json
{
  "iss": "https://dev-bxebbusy.us.auth0.com/",
  "sub": "Gpx90rLJMFNTyZ17J0RyqV3bUYfNNLsU@clients",
  "aud": "https://post-api.example.com/",
  "iat": 1652393037,
  "exp": 1652479437,
  "azp": "Gpx90rLJMFNTyZ17J0RyqV3bUYfNNLsU",
  "scope": "read:posts write:posts delete:posts",
  "gty": "client-credentials"
}
```

```bash
curl --request POST \
--url http://localhost:8080/posts \
--header 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkJaeGVKNE5CaHg4NlVqS3hqXzBvcyJ9.eyJpc3MiOiJodHRwczovL2Rldi1ieGViYnVzeS51cy5hdXRoMC5jb20vIiwic3ViIjoiR3B4OTByTEpNRk5UeVoxN0owUnlxVjNiVVlmTk5Mc1VAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vcG9zdC1hcGkuZXhhbXBsZS5jb20vIiwiaWF0IjoxNjUyMzkzMDM3LCJleHAiOjE2NTI0Nzk0MzcsImF6cCI6IkdweDkwckxKTUZOVHlaMTdKMFJ5cVYzYlVZZk5OTHNVIiwic2NvcGUiOiJyZWFkOnBvc3RzIHdyaXRlOnBvc3RzIGRlbGV0ZTpwb3N0cyIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.Hsx_8Bo7wP8UcrBt9XTiMRGrR5smo8kVuRLRKMHbgvCNM9fGjT-T7_Mk7L6qbNd6SXrGD8KhKnCPJ4gjMFZ1vL3Cl7x646aO1JL02F1lYG03kxydfXfaQhqQeYEHaRQ0-HitKTt-pegiJQNU1Y6b4M_Hrb9z800Uen8-IETlhnQJnDeo-xN5Zu36BzL4TR6EEk7Mdn8qbqihEdPc2T58oTx5k_5wC2UTG7KCIN8Bzw-mB0zliR6XI4oqvDLq9xwvS05uPeNIAsFVTfHZv49y0uotQUScVy71icwvNp3SoJ1O_xShSBA2aKSC8w251jOncRRCxy_5vpWtPnR0FNs27Q' \
--header 'Content-Type: application/json' \
--data '{"title": "New Post about Spring Secrity and Auth0", "content": "Content of this post, TBD"}'
```