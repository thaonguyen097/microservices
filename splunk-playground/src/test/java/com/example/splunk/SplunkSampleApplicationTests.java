package com.example.splunk;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;

import com.example.splunk.SplunkSampleApplication;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@SpringBootTest
class SplunkSampleApplicationTests {

	@Test
	void contextLoads() {
		assertDoesNotThrow(() -> SplunkSampleApplication.main(new String[]{"--server.port=0"}));
	}

}
