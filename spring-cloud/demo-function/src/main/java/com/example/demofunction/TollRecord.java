package com.example.demofunction;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TollRecord {

    private String stationId;
    private String licensePlate;
    private String timestamp;

}
