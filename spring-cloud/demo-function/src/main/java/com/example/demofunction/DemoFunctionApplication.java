package com.example.demofunction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@SpringBootApplication
public class DemoFunctionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoFunctionApplication.class, args);
	}

	List<TollStation> tollStations;

	public DemoFunctionApplication() {
		tollStations = new ArrayList<>();
		tollStations.add(new TollStation("100A", 123.5f, 2));
		tollStations.add(new TollStation("111B", 0.5f, 3));
		tollStations.add(new TollStation("222C", 12.5f, 4));
	}

	@Bean
	public Function<String, TollStation> retrieveStation() {
		return para -> tollStations.stream()
				.filter(ts -> para.equalsIgnoreCase(ts.getStationId()))
				.findAny()
				.orElse(null);
	}
	
	@Bean
	public Consumer<TollRecord> processTollRecord() {
		return para -> {
			System.out.println("received toll for car with license plate - " + para);
		};
	}

	@Bean
	public Function<TollRecord, Mono<Void>> processTollRecordReactive() {
		return para -> {
			System.out.println("received reactive toll for car with license plate - " + para);
			return Mono.empty();
		};
	}

	@Bean
	public Consumer<Flux<TollRecord>> processListOfTollRecordReactive() {
		return para -> para.subscribe(System.out::println);
	}

	@Bean
	public Supplier<Flux<TollStation>> getTollStations() {
		return () -> Flux.fromIterable(tollStations);
	}
}
