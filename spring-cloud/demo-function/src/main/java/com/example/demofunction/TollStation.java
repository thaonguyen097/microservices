package com.example.demofunction;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TollStation {

    private String stationId;
    private Float mileMarker;
    private Integer stallCount;

}
