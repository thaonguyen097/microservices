package com.example.customer;

import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CustomerApplicationTests {

	@Autowired
	ModelMapper modelMapper;

	@Test
	void contextLoads() {
		assertThat(modelMapper).isNotNull();
	}

	@Test
	public void applicationContextTest() {
		CustomerApplication.main(new String[] {});
	}

}
