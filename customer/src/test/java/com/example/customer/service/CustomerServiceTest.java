package com.example.customer.service;

import com.example.customer.model.dto.CustomerDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.customer.dao.CustomerRepo;
import com.example.customer.model.Customer;
import org.modelmapper.ModelMapper;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {
	
	@Mock
    private CustomerRepo repository;

    @Spy
    ModelMapper modelMapper=new ModelMapper();

    @InjectMocks
    private CustomerServiceImpl sut;

    private CustomerDto customerDto;
    private Customer customer;

    @BeforeEach
    void setup(){
//    	repository = Mockito.mock(CustomerRepo.class);
//    	sut = new CustomerServiceImpl(customerRepo);
        customer = Customer.builder()
                .id(1L)
                .name("donald@mail.com")
                .name("Donald")
                .build();

        customerDto = CustomerDto.builder()
                .id(1L)
                .name("donald@mail.com")
                .name("Donald")
                .build();
    }

    @DisplayName("JUnit test for save method")
    @Test
    void givenCustomer_whenSaveCustomer_thenReturnCustomerObject(){
        // given - precondition or setup
        given(repository.save(customer)).willReturn(customer);

        // when -  action or the behavior that we are going test
        CustomerDto saved = sut.save(customerDto);

        // then - verify the output
        assertThat(saved).isNotNull();
    }
    
    @DisplayName("JUnit test for save method which throws exception")
    @Test
    public void givenExistingEmail_whenSaveCustomer_thenThrowsException(){
        // given - precondition or setup
        given(repository.findByEmail(customer.getEmail()))
                .willReturn(Optional.of(customer));

        // when -  action or the behaviour that we are going test
        org.junit.jupiter.api.Assertions.assertThrows(IllegalArgumentException.class, () -> {
            sut.save(customerDto);
        });

        // then
        verify(repository, never()).save(any(Customer.class));
    }
    
    @DisplayName("JUnit test for findAll method")
    @Test
    void givenCustomerList_whenGetAllCustomer_thenReturnCustomerList(){
        // given - precondition or setup

        Customer customer2 = Customer.builder()
                .id(2L)
                .name("Henry")
                .build();

        given(repository.findAll()).willReturn(List.of(customer,customer2));

        // when -  action or the behavior that we are going test
        List<CustomerDto> list = sut.findAll();

        // then - verify the output
        assertThat(list).isNotNull();
        assertThat(list.size()).isEqualTo(2);
    }

    @DisplayName("JUnit test for findAll method")
    @Test
    void givenCustomerList_whenFindByName_thenReturnCustomerList(){
        // given - precondition or setup

        Customer customer2 = Customer.builder()
                .id(2L)
                .name("Henry")
                .build();

        given(repository.findByNameCustomQuery(anyString())).willReturn(List.of(customer2));

        // when -  action or the behavior that we are going test
        List<CustomerDto> list = sut.findByName("Henry");

        // then - verify the output
        assertThat(list).isNotNull();
        assertThat(list.size()).isEqualTo(1);
    }
    
    @DisplayName("JUnit test for findById method")
    @Test
    void givenCustomerId_whenGetById_thenReturnCustomerObject(){
        // given
        given(repository.findById(1L)).willReturn(Optional.of(customer));

        // when
        CustomerDto foundCustomer = sut.findById(customer.getId()).get();

        // then
        assertThat(foundCustomer).isNotNull();
    }
    
    @DisplayName("JUnit test for update method")
    @Test
    void givenCustomerObject_whenUpdate_thenReturnUpdatedCustomer(){
    	
        // given - precondition or setup
        given(repository.save(customer)).willReturn(customer);
        //customer.setEmail("sam@gmail.com");
        customer.setName("Sam");
        customerDto.setName("Sam");
        
        // when -  action or the behavior that we are going test
        CustomerDto updatedCustomer = sut.update(customerDto);

        // then - verify the output
//        assertThat(updatedCustomer.getEmail()).isEqualTo("sam@gmail.com");
        assertThat(updatedCustomer.getName()).isEqualTo("Sam");
    }
    
    @DisplayName("JUnit test for delete method")
    @Test
    public void givenCustomerId_whenDeleteCustomer_thenNothing(){
        // given - precondition or setup
        long customerId = 1L;

        willDoNothing().given(repository).deleteById(customerId);

        // when -  action or the behavior that we are going test
        sut.delete(customerId);

        // then - verify the output
        verify(repository, times(1)).deleteById(customerId);
    }

}
