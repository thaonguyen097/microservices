package com.example.customer.controller;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.example.customer.exception.ResourceNotFoundException;
import com.example.customer.model.dto.CustomerDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.web.servlet.MockMvc;

import com.example.customer.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.bind.MethodArgumentNotValidException;

@WebMvcTest(CustomerController.class)
class CustomerControllerTest {

	@MockBean
	private CustomerService customerService;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	CustomerDto customer = CustomerDto.builder().id(1l).name("Tom").email("tom@mail.com").build();
	
	@Test
	void shouldReturnListOfCustomer() throws Exception {
		List<CustomerDto> customers = Arrays.asList(
				CustomerDto.builder().id(1l).name("Tom").build(),
				CustomerDto.builder().id(2l).name("Jerry").build());
		when(customerService.findAll()).thenReturn(customers);
		mockMvc.perform(get("/api/customers"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.size()").value(customers.size()))
			.andExpect(jsonPath("$", hasSize(2)))
			.andExpect(jsonPath("$[1].name", is("Jerry")))
			.andDo(print());
	}

	@Test
	void shouldReturnListOfCustomer2() throws Exception {
		List<CustomerDto> customers = Arrays.asList(
				CustomerDto.builder().id(1l).name("tom").build());
		when(customerService.findByName("tom")).thenReturn(customers);
		mockMvc.perform(get("/api/customers/?name=tom"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.size()").value(customers.size()))
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].name", is("tom")))
				.andDo(print());
	}

	@Test
	void shouldReturnCustomer() throws Exception {
		long id = 1L;
		CustomerDto customer = CustomerDto.builder().id(1l).name("Jade").build();
		when(customerService.findById(id)).thenReturn(Optional.of(customer));
		mockMvc.perform(get("/api/customers/{id}", id))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.id").value(id))
			.andExpect(jsonPath("$.name").value(customer.getName()))
			.andDo(print());
	}

	@Test
	public void deletePatientById_success() throws Exception {
		when(customerService.findById(customer.getId())).thenReturn(Optional.of(customer));

		mockMvc.perform(
				delete("/api/customers/1")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isAccepted());
	}

	@Test
	public void deleteCustomerById_notFound() throws Exception {
		when(customerService.findById(5l)).thenReturn(Optional.empty());

		mockMvc.perform(
				delete("/api/customers/5")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(result ->
						assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
				.andExpect(result ->
						assertEquals("Customer id 5 not found.", result.getResolvedException().getMessage()));
	}

	@Test
	public void createRecord_success() throws Exception {

		when(customerService.save(customer)).thenReturn(customer);

		MockHttpServletRequestBuilder mockRequest = post("/api/customers")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(customer));

		mockMvc.perform(mockRequest)
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.name", is("Tom")));
	}

	@Test
	public void createRecord_fail() throws Exception {

		CustomerDto invalidOne = CustomerDto.builder().id(1l).name("1").build();

		MockHttpServletRequestBuilder mockRequest = post("/api/customers")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(invalidOne));

		mockMvc.perform(mockRequest)
				.andExpect(status().isBadRequest())
				.andExpect(result ->
						assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException));
	}

	@Test
	public void createRecord_fail_2() throws Exception {

		MockHttpServletRequestBuilder mockRequest = post("/api/customers")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content("something");

		mockMvc.perform(mockRequest)
				.andExpect(status().isBadRequest())
				.andExpect(result ->
						assertTrue(result.getResolvedException() instanceof HttpMessageNotReadableException));
	}

	@Test
	public void createRecord_fail_3() throws Exception {

		when(customerService.save(customer)).thenThrow(IllegalArgumentException.class);

		MockHttpServletRequestBuilder mockRequest = post("/api/customers")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(customer));

		mockMvc.perform(mockRequest)
				.andExpect(status().isInternalServerError())
				.andExpect(result ->
						assertTrue(result.getResolvedException() instanceof IllegalArgumentException));
	}

	@Test
	public void updatePatientRecord_success() throws Exception {

		CustomerDto updatedRecord = CustomerDto.builder().id(1l).name("Tom2").email("tom@mail.com").build();

		when(customerService.findById(customer.getId())).thenReturn(Optional.of(customer));
		when(customerService.update(updatedRecord)).thenReturn(updatedRecord);

		MockHttpServletRequestBuilder mockRequest = put("/api/customers/1")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(updatedRecord));

		mockMvc.perform(mockRequest)
				.andExpect(status().isAccepted())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.name", is("Tom2")));
	}

	@Test
	public void updatePatientRecord_recordNotFound() throws Exception {

		when(customerService.findById(customer.getId())).thenReturn(Optional.empty());

		MockHttpServletRequestBuilder mockRequest = put("/api/customers/1")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(customer));

		mockMvc.perform(mockRequest)
				.andExpect(status().isNotFound())
				.andExpect(result ->
						assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
				.andExpect(result ->
						assertEquals("Customer id 1 not found.", result.getResolvedException().getMessage()));
	}

}
