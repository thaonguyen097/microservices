package com.example.customer.dao;

import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;

import com.example.customer.model.Customer;

@DataJpaTest
@TestPropertySource(properties = { "spring.jpa.hibernate.ddl-auto=create-drop" })
@Sql("classpath:createCustomer.sql")
class CustomerRepoTests {

	@Autowired
	CustomerRepo sut;
	
//	private Customer one;
//	
//    @BeforeEach
//    public void setUp() {
//    	one = new Customer(1l, "Bat");
//    }
//    @AfterEach
//    public void tearDown() {
//        sut.deleteAll();
//        one = null;
//    }

	@Test
	void injectedComponentsAreNotNull() {
		assertThat(sut).isNotNull();
	}
	
	@Test
	void whenInitialized_thenFindAll() {
		List<Customer> customers = sut.findAll();
		assertThat(customers.size()).isEqualTo(2);
	}

	@Test
	void whenInitialized_thenFindsByName() {
		List<Customer> customers = sut.findByNameCustomQuery("Jane JC");
		assertThat(customers.size()).isEqualTo(1);
	}

}
