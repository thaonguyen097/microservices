package com.example.customer.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.customer.model.Customer;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Long> {

    /**
     * @param name
     * @return list customer
     */
    @Query("select u from Customer u where u.name like :name")
    List<Customer> findByNameCustomQuery(@Param("name") String name);

    /**
     * @param email
     * @return customer
     */
    Optional<Customer> findByEmail(String email);

}
