package com.example.customer;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CustomerApplication {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        SpringApplication.run(CustomerApplication.class, args);
    }

    /**
     * @return modelMapper
     */
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
