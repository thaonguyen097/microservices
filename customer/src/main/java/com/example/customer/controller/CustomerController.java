package com.example.customer.controller;

import java.util.List;

import com.example.customer.exception.ResourceNotFoundException;
import com.example.customer.model.dto.CustomerDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.customer.service.CustomerService;

import lombok.RequiredArgsConstructor;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/customers")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService service;

    /**
     * @param name
     * @return resp
     */
    @GetMapping
    public ResponseEntity<List<CustomerDto>> getAllCustomer(final @RequestParam(required = false) String name) {
        ResponseEntity<List<CustomerDto>> resp;
        if (StringUtils.hasLength(name)) {
            resp = ResponseEntity.status(HttpStatus.OK).body(service.findByName(name.trim()));
        } else {
            resp = ResponseEntity.status(HttpStatus.OK).body(service.findAll());
        }
        return resp;
    }

    /**
     * @param id
     * @return resp
     */
    @GetMapping("{customerId}")
    public ResponseEntity<CustomerDto> getCustomer(final @PathVariable("customerId") long id) {
        return service.findById(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * @param id
     * @param customer
     * @return resp
     */
    @PutMapping("{customerId}")
    public ResponseEntity<CustomerDto> updateCustomer(final @PathVariable("customerId") long id,
                                                   final @RequestBody @Valid CustomerDto customer) {
        return service.findById(id).map(dbCustomer -> {
            dbCustomer.setName(customer.getName());
            dbCustomer.setEmail(customer.getEmail());
            return ResponseEntity.accepted().body(service.update(dbCustomer));
        })
                .orElseThrow(() -> new ResourceNotFoundException("Customer id " + id + " not found."));
    }

    /**
     * @param id
     * @return resp
     */
    @DeleteMapping("{customerId}")
    public ResponseEntity<String> deleteEmployee(final @PathVariable("customerId") long id) {
        return service.findById(id).map(customer -> {
            service.delete(customer.getId());
            return ResponseEntity.accepted().body("Deleted");
        })
                .orElseThrow(() -> new ResourceNotFoundException("Customer id " + id + " not found."));
    }

    /**
     * @param customer
     * @return response
     */
    @PostMapping
    public ResponseEntity<CustomerDto> createEmployee(final @RequestBody @Valid CustomerDto customer) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.save(customer));
    }

}
