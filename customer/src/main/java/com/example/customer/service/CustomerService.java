package com.example.customer.service;

import com.example.customer.model.dto.CustomerDto;

import java.util.List;
import java.util.Optional;

public interface CustomerService {

    /**
     *
     * @return list customer
     */
    List<CustomerDto> findAll();

    /**
     *
     * @param name
     * @return list customer
     */
    List<CustomerDto> findByName(String name);

    /**
     *
     * @param id
     * @return customer
     */
    Optional<CustomerDto> findById(long id);

    /**
     *
     * @param customer
     * @return customer
     */
    CustomerDto save(CustomerDto customer);

    /**
     *
     * @param customer
     * @return customer
     */
    CustomerDto update(CustomerDto customer);

    /**
     *
     * @param customerId
     */
    void delete(long customerId);

}
