package com.example.customer.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.example.customer.model.dto.CustomerDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.customer.dao.CustomerRepo;
import com.example.customer.model.Customer;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepo customerRepo;
    private final ModelMapper modelMapper;

    /**
     * @return list customer
     */
    @Transactional(readOnly = true)
    @Override
    public List<CustomerDto> findAll() {
        return customerRepo.findAll()
                .stream().map(this::convertDto).collect(Collectors.toList());
    }

    /**
     * @param name
     * @return list customer
     */
    @Transactional(readOnly = true)
    @Override
    public List<CustomerDto> findByName(final String name) {
        return customerRepo.findByNameCustomQuery(name)
                .stream().map(this::convertDto).collect(Collectors.toList());
    }

    /**
     * @param id
     * @return customer
     */
    @Override
    public Optional<CustomerDto> findById(final long id) {
        return customerRepo.findById(id).map(this::convertDto);
    }

    /**
     * @param customer
     * @return customer
     */
    @Override
    public CustomerDto save(final CustomerDto customer) {

        final Optional<Customer> savedEmployee = customerRepo.findByEmail(customer.getEmail());
        if (savedEmployee.isPresent()) {
            throw new IllegalArgumentException("Customer already exist with given email:" + customer.getEmail());
        }
        return convertDto(customerRepo.save(convertEntity(customer)));
    }

    /**
     * @param customer
     * @return customer
     */
    @Override
    public CustomerDto update(final CustomerDto customer) {
        return convertDto(customerRepo.save(convertEntity(customer)));
    }

    /**
     * @param customerId
     */
    @Override
    public void delete(final long customerId) {
        customerRepo.deleteById(customerId);
    }

    private CustomerDto convertDto(final Customer customer) {
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.LOOSE);
        return modelMapper.map(customer, CustomerDto.class);
    }

    private Customer convertEntity(final CustomerDto customerDto) {
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.LOOSE);
        return modelMapper.map(customerDto, Customer.class);
    }

}
