package com.example.customer.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor @NoArgsConstructor @Builder
@Getter
public class ErrorMessage {

  private int statusCode;
  private LocalDate timestamp;
  private String message;
  private String description;

}
