package com.example.customer.exception;

public class ResourceNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     *
     * @param msg
     */
    public ResourceNotFoundException(final String msg) {
        super(msg);
    }
}
